extends Camera2D

const zoom_speed = 0.5


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _input(event):
	if event.is_action_pressed("scroll_up"):
			zoom.x = zoom.x - zoom_speed
			zoom.y = zoom.y - zoom_speed
	if event.is_action_pressed("scroll_down"):
			zoom.x = zoom.x + zoom_speed
			zoom.y = zoom.y + zoom_speed
